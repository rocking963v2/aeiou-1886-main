state={
	id=727
	name="STATE_727"
	manpower=5367
	state_category = small_island

	local_supplies=0.000

	history={
		owner = SPE
		buildings = {
			infrastructure = 1
			13048 = {
				naval_base = 1
			}
			13049 = {
				naval_base = 1
			}
			13053 = {
				naval_base = 1
			}
		}
	}
	
	provinces={
		13048 13049 13053 
	}
}
