state= {
	id=911
	name="STATE_911"
	manpower = 4356
	
	state_category = pastoral

	local_supplies=0.000

	history={
		owner = KWK
		add_core_of = KWK
		buildings = {
			infrastructure = 1
		}
	}
	provinces={
	   1393 1858 10273 10258 12231 10228 4870 1323 4385
	}
}