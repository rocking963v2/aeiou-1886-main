state= {
	id=908
	name="STATE_908"
	manpower = 4510
	
	state_category = rural

	local_supplies=0.000

	history={
		owner = COU
		add_core_of = COU
        victory_points = {
			4989 1 
		}
		buildings = {
			infrastructure = 1
		}
	}
	provinces={
	   13239 4989
	}
}