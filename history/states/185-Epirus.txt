
state={
	id=185
	name="STATE_185"
	manpower = 280000
	
	state_category = town
	
	resources={
		wood=14 # was: 20
	}

	local_supplies=0.000

	history={
		victory_points = {
			3914 3
		}
		victory_points = {
			1205 1
		}
		owner = GRE
		buildings = {
			infrastructure = 2
			industrial_complex = 1
			arms_factory = 1
			9805 = {
				naval_base = 1
			}
		}
		add_core_of = GRE
	}

	provinces={
		1205 3914 9805 9916 11895 12001 
	}
}
