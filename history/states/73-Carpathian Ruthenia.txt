state={
	id=73
	name="STATE_73"
	manpower = 398050
	
	state_category = rural
	
	local_supplies=0.000

	history={
		victory_points = {
			3548 1 
		}
		owner = AUS
		buildings = {
			infrastructure = 5
		}
		add_core_of = AUS
	}

	provinces={
		3548 6571 9563 11536 11691 
	}
}
